# Booking API #

This project is an API for making and manage reservations in hotels. It is build with Java 11 and Spring Boot

## Table of contents
* [Technologies](#technologies)
* [Launch](#launch)
  * [Warning](#warning)
* [Environment Variables (Optional)](#environment-variables-(optional))
* [Technologies](#technologies)

## Technologies

* Java 11
* Spring Boot 2.5.0
* Spring Data JPA
* PostgreSQL 13.3
* Log4j2
* Springfox 3.0.0
* Flyway 
* Sulky ULID
* ShedLock
* Jetty Web Server
* Gradle 7.0.2
* Docker

## Launch

In order to build and run this project is required **Gradle 6.8.x or 7.x**.

To simplify running this project, there is a **docker-compose file** in the root folder. It setups a **PostgreSQL 13.3**.   
If you have docker installed you can run the following command in the root folder of the project:

```bash
docker-compose up -d
```

### ***Warning***
If you do not use docker to set up the database, it is possible to configure the database connection
via environment variables.

After that, to start the project this is the command:

```bash
gradle bootRun
```

Finally, after the project is running it is necessary to execute the following call to the API:
```
curl --location --request POST 'http://localhost:7000/room/init' \
--data-raw ''
```
This request creates a room that is used by the reservations

### Environment Variables (Optional)
#### Service
* SERVICE_NAME (default: booking-service)
* SERVER_PORT (default: booking-service)
* CRON_RESERVATION_MANAGEMENT (default: 0 0 0 \* \* \*)

#### Business
* MAXIMUM_STAY_ALLOWED (default: 3)
* MAXIMUM_RESERVATION_ADVANCE (default: 30)

#### Datasource
* DATABASE_URL (default when using docker-compose: jdbc:postgresql://localhost:5432/booking_service)
* DATABASE_USERNAME (default when using docker-compose: booking_service)
* DATABASE_PASSWORD (default when using docker-compose: booking_service)

## How to run tests
To run the tests use the following command:
```bash
gradle test --info
```
### Utils
There are two Postman files in the root folder of the project that can help test the API manually.

### Who do I talk to? ###

* Ricardo da Silva Souza (ricasis.dasilvas@gmail.com)