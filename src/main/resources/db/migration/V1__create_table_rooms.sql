create table if not exists room(
    id varchar(26) primary key,
    number int not null,
    floor int not null,
    created_at timestamp not null,
    updated_at timestamp,
    unique(number, floor)
);