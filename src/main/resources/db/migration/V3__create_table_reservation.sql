create table if not exists reservation(
    id varchar(26) primary key,
    owner_reservation_id_document varchar(255) not null,
    owner_reservation_name varchar(255) not null,
    check_in timestamp not null,
    real_check_in timestamp,
    check_out timestamp not null,
    real_check_out timestamp,
    adults int not null,
    children int not null,
    status varchar(30) not null,
    room_id varchar(26) not null,
    created_at timestamp not null,
    updated_at timestamp,
    constraint fk_room foreign key(room_id) references room(id)
);