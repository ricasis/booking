create index concurrently idx_room_number on room(number);
create index concurrently idx_room_floor on room(floor);
create index concurrently idx_room_created_at on room(created_at);