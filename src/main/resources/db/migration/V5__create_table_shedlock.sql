create table shedlock (
  name varchar(64) primary key,
  lock_until timestamp(3) NULL,
  locked_at timestamp(3) NULL,
  locked_by varchar(255)
)