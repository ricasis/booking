package com.ricardo.booking.domain.repositories;

import com.ricardo.booking.domain.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, String>{

    String findByCheckInQuery =
        "select rm from Room rm " +
        "left join rm.reservations rs " +
        "on :checkIn between rs.checkIn and rs.checkOut " +
        "and rs.status in ('ACTIVE', 'CREATED') " +
        "where :checkIn not between rs.checkIn and rs.checkOut " +
        "or rs is null";

    @Query(findByCheckInQuery)
    List<Room> findByCheckIn(
        @Param("checkIn") LocalDate checkIn
    );

    @Query(findByCheckInQuery)
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<Room> findAndLockByCheckIn(
        @Param("checkIn") LocalDate checkIn
    );
}