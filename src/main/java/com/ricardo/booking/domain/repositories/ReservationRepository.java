package com.ricardo.booking.domain.repositories;

import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.entities.enums.ReservationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, String> {

    List<Reservation> findByOwnerReservationIdDocumentAndStatusNot(
        String ownerReservationIdDocument,
        ReservationStatus status
    );

    @Query(
        "select r from Reservation r " +
        "where r.status = 'ACTIVE' " +
        "and r.ownerReservationIdDocument =:ownerReservIdDoc " +
        "and r.checkIn =:checkIn " +
        "and r.checkOut =:checkOut"
    )
    Reservation findByOwnerIdDocAndDateAndStatus(
        @Param("ownerReservIdDoc") String ownerReservIdDoc,
        @Param("checkIn") LocalDate checkIn,
        @Param("checkOut") LocalDate checkOut
    );

    @Query(
        "select r from Reservation r " +
        "where r.status = 'CREATED' " +
        "and r.checkIn =:date"
    )
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    List<Reservation> findAllMustExpire(@Param("date") LocalDate date);
}
