package com.ricardo.booking.domain.entities;

import com.ricardo.booking.application.web.payloads.requests.BookingRequest;
import com.ricardo.booking.domain.entities.enums.ReservationStatus;
import de.huxhorn.sulky.ulid.ULID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    private String id;

    @Column(name = "owner_reservation_id_document")
    private String ownerReservationIdDocument;

    @Column(name = "owner_reservation_name")
    private String ownerReservationName;

    @Column(name="check_in")
    private LocalDate checkIn;

    @Column(name="real_check_in")
    private LocalDate realCheckIn;

    @Column(name="check_out")
    private LocalDate checkOut;

    @Column(name="real_check_out")
    private LocalDate realCheckOut;

    @Column
    private Integer adults;

    @Column
    private Integer children;

    @Column
    @Enumerated(EnumType.STRING)
    private ReservationStatus status;

    @Column(name="created_at")
    private LocalDateTime createdAt;

    @Column(name="updated_at")
    private LocalDateTime updatedAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="room_id")
    private Room room;

    public Reservation() {}

    public Reservation(
        String id,
        String ownerReservationIdDocument,
        String ownerReservationName,
        LocalDate checkIn,
        LocalDate realCheckIn,
        LocalDate checkOut,
        LocalDate realCheckOut,
        Integer adults,
        Integer children,
        ReservationStatus status,
        LocalDateTime createdAt,
        LocalDateTime updatedAt,
        Room room
    ) {
        this.id = id;
        this.ownerReservationIdDocument = ownerReservationIdDocument;
        this.ownerReservationName = ownerReservationName;
        this.checkIn = checkIn;
        this.realCheckIn = realCheckIn;
        this.checkOut = checkOut;
        this.realCheckOut = realCheckOut;
        this.adults = adults;
        this.children = children;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.room = room;
    }

    public Reservation(Room room, BookingRequest booking) {
        ULID ulid = new ULID();

        this.id = ulid.nextULID();
        this.ownerReservationName = booking.getOwnerReservationName();
        this.ownerReservationIdDocument = booking.getOwnerReservationIdDocument();
        this.checkIn = booking.getCheckIn();
        this.checkOut = booking.getCheckOut();
        this.adults = booking.getAdults();
        this.children = booking.getChildren();
        this.room = room;
        this.createdAt = LocalDateTime.now();
        this.status = ReservationStatus.CREATED;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getOwnerReservationIdDocument() {
        return ownerReservationIdDocument;
    }

    public void setOwnerReservationIdDocument(String ownerReservationIdDocument) {
        this.ownerReservationIdDocument = ownerReservationIdDocument;
    }

    public String getOwnerReservationName() {
        return ownerReservationName;
    }

    public void setOwnerReservationName(String ownerReservationName) {
        this.ownerReservationName = ownerReservationName;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public LocalDate getRealCheckIn() {
        return realCheckIn;
    }

    public void setRealCheckIn(LocalDate realCheckIn) {
        this.realCheckIn = realCheckIn;
    }

    public LocalDate getRealCheckOut() {
        return realCheckOut;
    }

    public void setRealCheckOut(LocalDate realCheckOut) {
        this.realCheckOut = realCheckOut;
    }
}
