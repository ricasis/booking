package com.ricardo.booking.domain.entities.enums;

import java.util.Collections;
import java.util.List;

public enum ReservationStatus {
    INACTIVE(
        Collections.EMPTY_LIST
    ),
    EXPIRED(
        Collections.EMPTY_LIST
    ),
    CANCELED(
        Collections.EMPTY_LIST
    ),
    ACTIVE(
        List.of(INACTIVE)
    ),
    CREATED(
        List.of(ACTIVE, EXPIRED, CANCELED)
    );

    private static final List<ReservationStatus> statusAllowedToUpdate =
        List.of(CREATED);

    private final List<ReservationStatus> next;


    ReservationStatus(List<ReservationStatus> next) {
        this.next = next;
    }

    public boolean isNext(ReservationStatus status) {
        return this.next.contains(status);
    }

    public boolean canUpdate() {
        return statusAllowedToUpdate.contains(this);
    }
}
