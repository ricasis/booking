package com.ricardo.booking.domain.services;

import com.ricardo.booking.application.web.payloads.requests.BookingRequest;
import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.exceptions.ApiException;

import java.time.LocalDate;
import java.util.List;

public interface ReservationService {
    Reservation makeReservation(BookingRequest bookingRequest) throws ApiException;
    Reservation updateReservation(String reservationId, BookingRequest bookingRequest) throws ApiException;
    void cancelReservation(String reservationId) throws ApiException;
    List<Reservation> getActiveReservationByOwnerIdDoc(String idDocument);
    void manageReservations(LocalDate today);
    Reservation doCheckIn(String reservationId, LocalDate checkIn) throws ApiException;
    Reservation doCheckOut(String reservationId, LocalDate checkOut) throws ApiException;
}
