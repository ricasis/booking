package com.ricardo.booking.domain.services.impl;

import com.ricardo.booking.application.configs.logging.Logable;
import com.ricardo.booking.application.web.payloads.requests.BookingRequest;
import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.entities.Room;
import com.ricardo.booking.domain.entities.enums.ReservationStatus;
import com.ricardo.booking.domain.exceptions.ApiException;
import com.ricardo.booking.domain.exceptions.MaximumReservAdvanceExceedException;
import com.ricardo.booking.domain.exceptions.MaximumStayExceedException;
import com.ricardo.booking.domain.exceptions.NoRoomAvailableException;
import com.ricardo.booking.domain.exceptions.ReservationNotFoundException;
import com.ricardo.booking.domain.exceptions.UpdateNotAllowedException;
import com.ricardo.booking.domain.repositories.ReservationRepository;
import com.ricardo.booking.domain.repositories.RoomRepository;
import com.ricardo.booking.domain.services.ReservationService;
import com.ricardo.booking.domain.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl extends Logable implements ReservationService {

    private final RoomRepository roomRepository;
    private final ReservationRepository reservationRepository;
    private final Integer maximumStayAllowed;
    private final Integer maximumReservationInAdvance;

    public ReservationServiceImpl(
        RoomRepository roomRepository,
        ReservationRepository reservationRepository,
        @Value("${maximum.stay.allowed}")
        Integer maximumStayAllowed,
        @Value("${maximum.reservation.advance}")
        Integer maximumReservationInAdvance
    ) {
        this.roomRepository = roomRepository;
        this.reservationRepository = reservationRepository;
        this.maximumStayAllowed = maximumStayAllowed;
        this.maximumReservationInAdvance = maximumReservationInAdvance;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Reservation makeReservation(BookingRequest bookingRequest) throws ApiException {
        try {
            validateBooking(bookingRequest);

            Reservation reservation;

            reservation = reservationRepository.findByOwnerIdDocAndDateAndStatus(
                bookingRequest.getOwnerReservationIdDocument(),
                bookingRequest.getCheckIn(),
                bookingRequest.getCheckOut()
            );

            if (reservation == null) {
                List<Room> availableRooms = roomRepository.findAndLockByCheckIn(
                    bookingRequest.getCheckIn()
                ).stream().distinct().collect(Collectors.toList());

                if (availableRooms.isEmpty()) throw new NoRoomAvailableException();

                Room room = availableRooms.get(0);
                reservation = new Reservation(room, bookingRequest);
                reservation = reservationRepository.save(reservation);
            }

            return reservation;
        } catch (Exception e) {
            logger.error(
                String.format("Error trying to book a room: %s -> %s", e.getMessage(), e)
            );
            throw e;
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Reservation updateReservation(String reservationId, BookingRequest bookingRequest) throws ApiException {
        try {

            Optional<Reservation> opReservation = reservationRepository.findById(reservationId);
            if (opReservation.isEmpty()) throw new ReservationNotFoundException(reservationId);

            Reservation reservation = opReservation.get();

            if (!reservation.getStatus().canUpdate())
                throw new UpdateNotAllowedException(reservation.getStatus());

            validateBooking(bookingRequest);

            Room room = null;

            if (!reservation.getCheckIn().isEqual(bookingRequest.getCheckIn()) ||
                !reservation.getCheckOut().isEqual(bookingRequest.getCheckOut())) {

                List<Room> availableRooms = roomRepository.findAndLockByCheckIn(
                    bookingRequest.getCheckIn()
                );

                if (availableRooms.isEmpty()) throw new NoRoomAvailableException();

                room = availableRooms.get(0);
            }

            reservation.setOwnerReservationIdDocument(bookingRequest.getOwnerReservationIdDocument());
            reservation.setOwnerReservationName(bookingRequest.getOwnerReservationName());
            reservation.setCheckIn(bookingRequest.getCheckIn());
            reservation.setCheckOut(bookingRequest.getCheckOut());
            reservation.setAdults(bookingRequest.getAdults());
            reservation.setChildren(bookingRequest.getChildren());

            if (room != null) {
                reservation.setRoom(room);
            }

            reservation = reservationRepository.save(reservation);

            return reservation;
        } catch (Exception e) {
            logger.error(
                String.format(
                    "Error trying to update reservation that has id %s: %s -> %s", reservationId, e.getMessage(), e
                )
            );
            throw e;
        }
    }

    @Override
    public void cancelReservation(String reservationId) throws ApiException {
        try {
            Optional<Reservation> opReservation = reservationRepository.findById(reservationId);
            if (opReservation.isEmpty()) throw new ReservationNotFoundException(reservationId);

            Reservation reservation = opReservation.get();

            if (!reservation.getStatus().isNext(ReservationStatus.CANCELED))
                throw new IllegalArgumentException("Reservation can not be cancelled");

            reservation.setStatus(ReservationStatus.CANCELED);
            reservationRepository.save(reservation);
        } catch (Exception e) {
            logger.error(
                String.format(
                    "Error trying to cancel reservation that has id %s: %s -> %s", reservationId, e.getMessage(), e
                )
            );
            throw e;
        }
    }

    @Override
    public List<Reservation> getActiveReservationByOwnerIdDoc(String idDocument) {
        return reservationRepository.findByOwnerReservationIdDocumentAndStatusNot(
            idDocument, ReservationStatus.CANCELED
        );
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void manageReservations(LocalDate today) {
        try {
            logger.info("Getting reservations...");
            List<Reservation> reservations = reservationRepository.findAllMustExpire(
                today.minus(2, ChronoUnit.DAYS)
            );
            logger.info(
                String.format("%d reservations found", reservations.size())
            );
            reservations.forEach( r -> {
                r.setStatus(ReservationStatus.EXPIRED);
                logger.info(
                    String.format("Reservation %s is expired", r.getId())
                );
                reservationRepository.save(r);
            });
        } catch (Exception e) {
            logger.error(
                String.format("Error trying to process expired reservations: %s -> %s", e.getMessage(), e)
            );
            throw e;
        }
    }

    @Override
    public Reservation doCheckIn(String reservationId, LocalDate checkIn) throws ApiException {
        try {
            return doCheckInOut(reservationId, checkIn, true);
        } catch (Exception e) {
            logger.error(
                String.format(
                    "Error trying to do check-in for reservation that has id %s: %s -> %s",
                    reservationId, e.getMessage(), e
                )
            );
            throw e;
        }
    }

    @Override
    public Reservation doCheckOut(String reservationId, LocalDate checkOut) throws ApiException {
        try {
            return doCheckInOut(reservationId, checkOut, false);
        } catch (Exception e) {
            logger.error(
                String.format(
                    "Error trying to do check-out for reservation that has id %s: %s -> %s",
                    reservationId, e.getMessage(), e
                )
            );
            throw e;
        }
    }

    private Reservation doCheckInOut(String reservationId, LocalDate date, boolean isCheckIn) throws ApiException {
        Optional<Reservation> opReservation = reservationRepository.findById(reservationId);
        if (opReservation.isEmpty()) throw new ReservationNotFoundException(reservationId);

        Reservation reservation = opReservation.get();
        ReservationStatus status = reservation.getStatus();

        if (isCheckIn) {
            if (reservation.getCheckIn().isAfter(date))
                throw new IllegalArgumentException("Check-in is not allowed before booked date");

            if (!status.isNext(ReservationStatus.ACTIVE))
                throw new IllegalArgumentException("Check-in is not allowed for this reservation");

            reservation.setStatus(ReservationStatus.ACTIVE);
            reservation.setRealCheckIn(date);
        } else {
            if (!status.isNext(ReservationStatus.INACTIVE))
                throw new IllegalArgumentException("Check-out is not allowed for this reservation");

            reservation.setStatus(ReservationStatus.INACTIVE);
            reservation.setRealCheckOut(date);
        }

        reservation.setUpdatedAt(LocalDateTime.now());

        return reservationRepository.save(reservation);
    }

    private void validateBooking(BookingRequest booking) throws ApiException {

        if (ChronoUnit.DAYS.between(LocalDate.now(), booking.getCheckIn()) < 0) {
            throw new IllegalArgumentException("Check in date must be equal or after today");
        }

        if (ChronoUnit.DAYS.between(booking.getCheckIn(), booking.getCheckOut()) < 0) {
            throw new IllegalArgumentException("Check out date must be after check in date");
        }

        if (ChronoUnit.DAYS.between(booking.getCheckIn(), booking.getCheckOut()) > maximumStayAllowed) {
            throw new MaximumStayExceedException(maximumStayAllowed);
        }

        if (ChronoUnit.DAYS.between(LocalDate.now(), booking.getCheckIn()) > maximumReservationInAdvance) {
            throw new MaximumReservAdvanceExceedException(maximumReservationInAdvance);
        }
    }
}
