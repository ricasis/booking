package com.ricardo.booking.domain.services;

import com.ricardo.booking.domain.entities.Room;

import java.time.LocalDate;
import java.util.List;

public interface RoomService {

    Room initRooms();
    List<Room> findAvailableByCheckIn(LocalDate checkIn);
}
