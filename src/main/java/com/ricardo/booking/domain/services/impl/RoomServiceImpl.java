package com.ricardo.booking.domain.services.impl;

import com.ricardo.booking.application.configs.logging.Logable;
import com.ricardo.booking.domain.entities.Room;
import com.ricardo.booking.domain.repositories.RoomRepository;
import com.ricardo.booking.domain.services.RoomService;
import de.huxhorn.sulky.ulid.ULID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class RoomServiceImpl extends Logable implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public List<Room> findAvailableByCheckIn(LocalDate checkIn) {
        return roomRepository.findByCheckIn(checkIn);
    }

    @Override
    public Room initRooms() {
        if (roomRepository.count() < 1L) {
            ULID ulid = new ULID();
            Room room = new Room(
                ulid.nextULID(),
                1,
                1,
                LocalDateTime.now(),
                null,
                null
            );
            return roomRepository.save(room);
        }
        return roomRepository.findAll().get(0);
    }
}
