package com.ricardo.booking.domain.exceptions;

import org.springframework.http.HttpStatus;

public class MaximumReservAdvanceExceedException extends ApiException {

    public MaximumReservAdvanceExceedException(Integer maxReservAdvance) {
        super(
            String.format("It is not allowed to book a room more than %d days in advance", maxReservAdvance),
            HttpStatus.BAD_REQUEST
        );
    }
}
