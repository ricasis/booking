package com.ricardo.booking.domain.exceptions;

import org.springframework.http.HttpStatus;

public class NoRoomAvailableException extends ApiException {

    public NoRoomAvailableException() {
        super("There is not room available", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
