package com.ricardo.booking.domain.exceptions;

import com.ricardo.booking.domain.entities.enums.ReservationStatus;
import org.springframework.http.HttpStatus;

public class UpdateNotAllowedException extends ApiException {

    public UpdateNotAllowedException(ReservationStatus status) {
        super(
            String.format("It is not allowed to update reservation in status %s", status.name()),
            HttpStatus.BAD_REQUEST
        );
    }
}
