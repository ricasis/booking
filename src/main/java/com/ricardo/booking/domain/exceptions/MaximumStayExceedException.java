package com.ricardo.booking.domain.exceptions;

import org.springframework.http.HttpStatus;

public class MaximumStayExceedException extends ApiException {

    public MaximumStayExceedException(Integer maxStayAllowed) {
        super(
            String.format("It is not allowed to book a room for more than %d", maxStayAllowed),
            HttpStatus.BAD_REQUEST
        );
    }
}
