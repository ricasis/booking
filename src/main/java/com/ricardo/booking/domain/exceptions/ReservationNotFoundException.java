package com.ricardo.booking.domain.exceptions;

import org.springframework.http.HttpStatus;

public class ReservationNotFoundException extends ApiException {

    public ReservationNotFoundException(String reservationId) {
        super(
            String.format("There is not reservation with id: %s", reservationId),
            HttpStatus.BAD_REQUEST
        );
    }
}
