package com.ricardo.booking.application.web.payloads.responses;

import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.entities.enums.ReservationStatus;

import java.time.LocalDate;

public class BookingResponse {

    private String id;
    private String ownerReservationIdDocument;
    private String ownerReservationName;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private String roomId;
    private Integer roomNumber;
    private Integer roomFloor;
    private Integer adults;
    private Integer children;
    private ReservationStatus reservationStatus;

    public BookingResponse(Reservation reservation) {
        this.id = reservation.getId();
        this.ownerReservationName = reservation.getOwnerReservationName();
        this.ownerReservationIdDocument = reservation.getOwnerReservationIdDocument();
        this.checkIn = reservation.getCheckIn();
        this.checkOut = reservation.getCheckOut();
        this.roomId = reservation.getRoom().getId();
        this.roomNumber = reservation.getRoom().getNumber();
        this.roomFloor = reservation.getRoom().getFloor();
        this.adults = reservation.getAdults();
        this.children = reservation.getChildren();
        this.reservationStatus = reservation.getStatus();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerReservationIdDocument() {
        return ownerReservationIdDocument;
    }

    public void setOwnerReservationIdDocument(String ownerReservationIdDocument) {
        this.ownerReservationIdDocument = ownerReservationIdDocument;
    }

    public String getOwnerReservationName() {
        return ownerReservationName;
    }

    public void setOwnerReservationName(String ownerReservationName) {
        this.ownerReservationName = ownerReservationName;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getRoomFloor() {
        return roomFloor;
    }

    public void setRoomFloor(Integer roomFloor) {
        this.roomFloor = roomFloor;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }
}
