package com.ricardo.booking.application.web.payloads.responses;

import com.ricardo.booking.domain.entities.Reservation;

import java.time.LocalDate;

public class CheckInOutResponse {

    private String id;
    private String ownerReservationIdDocument;
    private String ownerReservationName;
    private LocalDate realCheckIn;
    private LocalDate realCheckOut;
    private String roomId;
    private Integer roomNumber;
    private Integer roomFloor;
    private Integer adults;
    private Integer children;

    public CheckInOutResponse(Reservation reservation) {
        this.id = reservation.getId();
        this.ownerReservationIdDocument = reservation.getOwnerReservationIdDocument();
        this.ownerReservationName = reservation.getOwnerReservationName();
        this.realCheckIn = reservation.getRealCheckIn();
        this.realCheckOut = reservation.getRealCheckOut();
        this.roomId = reservation.getRoom().getId();
        this.roomNumber = reservation.getRoom().getNumber();
        this.roomFloor = reservation.getRoom().getFloor();
        this.adults = reservation.getAdults();
        this.children = reservation.getChildren();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerReservationIdDocument() {
        return ownerReservationIdDocument;
    }

    public void setOwnerReservationIdDocument(String ownerReservationIdDocument) {
        this.ownerReservationIdDocument = ownerReservationIdDocument;
    }

    public String getOwnerReservationName() {
        return ownerReservationName;
    }

    public void setOwnerReservationName(String ownerReservationName) {
        this.ownerReservationName = ownerReservationName;
    }

    public LocalDate getRealCheckIn() {
        return realCheckIn;
    }

    public void setRealCheckIn(LocalDate realCheckIn) {
        this.realCheckIn = realCheckIn;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Integer getRoomFloor() {
        return roomFloor;
    }

    public void setRoomFloor(Integer roomFloor) {
        this.roomFloor = roomFloor;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public LocalDate getRealCheckOut() {
        return realCheckOut;
    }

    public void setRealCheckOut(LocalDate realCheckOut) {
        this.realCheckOut = realCheckOut;
    }
}
