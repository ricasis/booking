package com.ricardo.booking.application.web.payloads.requests;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class BookingRequest {

    @NotNull(message = "Owner reservation's identification document is mandatory")
    private String ownerReservationIdDocument;

    @NotNull(message = "Owner reservation's name is mandatory")
    private String ownerReservationName;

    @NotNull(message = "CheckIn is mandatory")
    private LocalDate checkIn;

    @NotNull(message = "CheckIn is mandatory")
    private LocalDate checkOut;

    @Min(value = 1, message = "It is mandatory at least 1 adult in the booking request")
    private Integer adults;

    private Integer children;

    public BookingRequest(
        String ownerReservationIdDocument,
        String ownerReservationName,
        LocalDate checkIn,
        LocalDate checkOut,
        Integer adults,
        Integer children
    ) {
        this.ownerReservationIdDocument = ownerReservationIdDocument;
        this.ownerReservationName = ownerReservationName;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.adults = adults;
        this.children = children;
    }

    public LocalDate getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(LocalDate checkIn) {
        this.checkIn = checkIn;
    }

    public LocalDate getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(LocalDate checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getAdults() {
        return adults;
    }

    public void setAdults(Integer adults) {
        this.adults = adults;
    }

    public Integer getChildren() {
        return children;
    }

    public void setChildren(Integer children) {
        this.children = children;
    }

    public String getOwnerReservationIdDocument() {
        return ownerReservationIdDocument;
    }

    public void setOwnerReservationIdDocument(String ownerReservationIdDocument) {
        this.ownerReservationIdDocument = ownerReservationIdDocument;
    }

    public String getOwnerReservationName() {
        return ownerReservationName;
    }

    public void setOwnerReservationName(String ownerReservationName) {
        this.ownerReservationName = ownerReservationName;
    }
}
