package com.ricardo.booking.application.web.controllers;

import com.ricardo.booking.application.configs.logging.Logable;
import com.ricardo.booking.application.web.payloads.responses.RoomResponse;
import com.ricardo.booking.domain.entities.Room;
import com.ricardo.booking.domain.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(
    path = "/room",
    produces = MediaType.APPLICATION_JSON_VALUE
)
public class RoomController extends Logable {

    @Autowired
    private RoomService roomService;

    @GetMapping("/available/{checkIn}")
    private List<RoomResponse> findAllAvailable(
        @PathVariable("checkIn") String checkIn
    ) {
        logger.info(
            String.format("Received request to check rooms available in %s", checkIn)
        );
        List<RoomResponse> response = new ArrayList<>();
        roomService.findAvailableByCheckIn(LocalDate.parse(checkIn)).forEach( room -> {
            response.add(new RoomResponse(room));
        });
        logger.info(
            String.format("There is %d room(s) available", response.size())
        );
        return response;
    }

    @PostMapping("/init")
    private RoomResponse init() {
        logger.info("Init rooms request received...");

        Room room = roomService.initRooms();

        logger.info(
            String.format("Room created with id: %s", room.getId())
        );
        return new RoomResponse(room);
    }
}
