package com.ricardo.booking.application.web.controllers;

import com.ricardo.booking.application.configs.logging.Logable;
import com.ricardo.booking.application.web.payloads.requests.BookingRequest;
import com.ricardo.booking.application.web.payloads.responses.BookingResponse;
import com.ricardo.booking.application.web.payloads.responses.CheckInOutResponse;
import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.exceptions.ApiException;
import com.ricardo.booking.domain.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(
    path = "/reservation",
    produces = MediaType.APPLICATION_JSON_VALUE,
    consumes = MediaType.APPLICATION_JSON_VALUE
)
public class ReservationController extends Logable {

    @Autowired
    private ReservationService reservationService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookingResponse makeReservation(
        @Valid @RequestBody BookingRequest bookRequest
    ) throws ApiException {
        logger.info("Booking request received...");

        Reservation reservation = reservationService.makeReservation(bookRequest);

        logger.info(String.format("Booking request processed. Reservation Id %s", reservation.getId()));

        return new BookingResponse(reservation);
    }

    @PostMapping("/{reservationId}/check-in")
    public CheckInOutResponse doCheckIn(
        @PathVariable("reservationId") String reservationId
    ) throws ApiException {
        logger.info(
            String.format("Request to check-in reservation %s received...", reservationId)
        );

        Reservation reservation = reservationService.doCheckIn(reservationId, LocalDate.now());

        logger.info(String.format("Check-in successfully done. Reservation Id %s", reservation.getId()));

        return new CheckInOutResponse(reservation);
    }

    @PostMapping("/{reservationId}/check-out")
    public CheckInOutResponse doCheckOut(
        @PathVariable("reservationId") String reservationId
    ) throws ApiException {
        logger.info(
            String.format("Request to check-out reservation %s received...", reservationId)
        );

        Reservation reservation = reservationService.doCheckOut(reservationId, LocalDate.now());

        logger.info(String.format("Check-out successfully done. Reservation Id %s", reservation.getId()));

        return new CheckInOutResponse(reservation);
    }

    @PostMapping("/{reservationId}/cancel")
    public void cancelReservation(
        @PathVariable("reservationId") String reservationId
    ) throws ApiException {
        logger.info(
            String.format("Canceling reservation request received for reservationId: %s", reservationId)
        );

        reservationService.cancelReservation(reservationId);

        logger.info(
            String.format("Reservation %s successfully canceled", reservationId)
        );
    }

    @GetMapping("/owner/{ownerIdDocument}")
    public List<BookingResponse> getReservation(
        @PathVariable("ownerIdDocument") String idDocument
    ) {
        logger.info(
            String.format("Getting reservation which has owner's identification document: %s", idDocument)
        );

        List<BookingResponse> response = new ArrayList<>();

        reservationService.getActiveReservationByOwnerIdDoc(idDocument).forEach(reservation -> {
            response.add(new BookingResponse(reservation));
        });

        return response;
    }

    @PatchMapping("/{reservationId}")
    public BookingResponse updateReservation(
        @PathVariable("reservationId") String reservationId,
        @Valid @RequestBody BookingRequest bookingRequest
    ) throws ApiException {
        logger.info(
            String.format("Updating reservation request received for reservation with id: %s", reservationId)
        );

        Reservation reservation = reservationService.updateReservation(reservationId, bookingRequest);

        logger.info(
            String.format("Reservation with id: %s successfully updated", reservation.getId())
        );

        return new BookingResponse(reservation);
    }
}
