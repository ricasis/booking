package com.ricardo.booking.application.web.payloads.responses;

import com.ricardo.booking.domain.entities.Room;

public class RoomResponse {

    private String id;
    private Integer number;
    private Integer floor;

    public RoomResponse(Room room) {
        this.id = room.getId();
        this.number = room.getNumber();
        this.floor = room.getFloor();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }
}
