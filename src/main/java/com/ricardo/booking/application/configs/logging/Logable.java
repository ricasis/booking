package com.ricardo.booking.application.configs.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.invoke.MethodHandles;

public abstract class Logable {
    protected static final Logger logger = LogManager.getLogger(MethodHandles.lookup().lookupClass());
}
