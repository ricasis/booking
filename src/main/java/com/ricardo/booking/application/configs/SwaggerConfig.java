package com.ricardo.booking.application.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

    @Value("${service.name}")
    private String serviceName;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.ricardo.booking.application.web.controllers"))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(
                new ApiInfoBuilder()
                .title(serviceName)
                .description("API to manage bookings")
                .version("0.0.1")
                .build()
            );
    }
}
