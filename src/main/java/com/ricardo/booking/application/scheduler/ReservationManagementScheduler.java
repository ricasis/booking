package com.ricardo.booking.application.scheduler;

import com.ricardo.booking.application.configs.logging.Logable;
import com.ricardo.booking.domain.services.ReservationService;
import com.ricardo.booking.domain.services.RoomService;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class ReservationManagementScheduler extends Logable {

    @Autowired
    private ReservationService reservationService;

    @Scheduled(cron = "${cron.reservation.management}")
    @SchedulerLock(name = "reservationManagement", lockAtLeastFor = "10s")
    public void manageRooms() throws Exception {
        try {
            logger.info("Starting management...");

            LocalDate today = LocalDate.now();
            reservationService.manageReservations(today);

            logger.info("Management successfully finished");
        } catch (Exception e) {
            logger.error(
                String.format("Error managing reservations: %s -> %s", e.getMessage(), e)
            );
            throw e;
        }
    }
}
