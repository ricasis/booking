package com.ricardo.booking.domain.services;

import com.ricardo.booking.application.factories.BookingRequestFactory;
import com.ricardo.booking.application.factories.ReservationFactory;
import com.ricardo.booking.application.factories.RoomFactory;
import com.ricardo.booking.application.web.payloads.requests.BookingRequest;
import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.entities.Room;
import com.ricardo.booking.domain.entities.enums.ReservationStatus;
import com.ricardo.booking.domain.exceptions.ApiException;
import com.ricardo.booking.domain.exceptions.MaximumReservAdvanceExceedException;
import com.ricardo.booking.domain.exceptions.MaximumStayExceedException;
import com.ricardo.booking.domain.exceptions.NoRoomAvailableException;
import com.ricardo.booking.domain.exceptions.ReservationNotFoundException;
import com.ricardo.booking.domain.exceptions.UpdateNotAllowedException;
import com.ricardo.booking.domain.repositories.ReservationRepository;
import com.ricardo.booking.domain.repositories.RoomRepository;
import com.ricardo.booking.domain.services.impl.ReservationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReservationServiceTest {

    @Mock
    private RoomRepository roomRepository;

    @Mock
    private ReservationRepository reservationRepository;

    @Captor
    private ArgumentCaptor<Reservation> reservationCaptor;

    private ReservationServiceImpl reservationService;

    @BeforeEach
    public void init() {
        reservationService = new ReservationServiceImpl(
            roomRepository,
            reservationRepository,
            3,
            30
        );
    }


    @Test
    public void givenNewBooking_WhenValid_ItShouldBeSaved() throws ApiException {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        List<Room> expectedRooms = List.of(RoomFactory.sample());

        when(
            reservationRepository.findByOwnerIdDocAndDateAndStatus(
                any(String.class), any(LocalDate.class), any(LocalDate.class)
            )
        ).thenReturn(null);

        when(
            roomRepository.findAndLockByCheckIn(any(LocalDate.class))
        ).thenReturn(expectedRooms);

        when(
            reservationRepository.save(any(Reservation.class))
        ).thenReturn(null);

        reservationService.makeReservation(expectedBookingRequest);

        Mockito.verify(reservationRepository).save(reservationCaptor.capture());
        Reservation result = reservationCaptor.getValue();

        assertEquals(result.getCheckIn(), expectedBookingRequest.getCheckIn());
        assertEquals(result.getCheckOut(), expectedBookingRequest.getCheckOut());
        assertEquals(result.getAdults(), expectedBookingRequest.getAdults());
        assertEquals(result.getChildren(), expectedBookingRequest.getChildren());
        assertEquals(result.getOwnerReservationName(), expectedBookingRequest.getOwnerReservationName());
        assertEquals(result.getOwnerReservationIdDocument(), expectedBookingRequest.getOwnerReservationIdDocument());
    }

    @Test
    public void givenNewBooking_WhenBookingCheckInInvalid_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        expectedBookingRequest.setCheckIn(LocalDate.now().minus(1, ChronoUnit.DAYS));

        assertThrows(IllegalArgumentException.class, () -> {
            reservationService.makeReservation(expectedBookingRequest);
        });
    }

    @Test
    public void givenNewBooking_WhenBookingCheckOutInvalid_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        expectedBookingRequest.setCheckOut(LocalDate.now().minus(1, ChronoUnit.DAYS));

        assertThrows(IllegalArgumentException.class, () -> {
            reservationService.makeReservation(expectedBookingRequest);
        });
    }

    @Test
    public void givenNewBooking_WhenStayRangeDateInvalid_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        expectedBookingRequest.setCheckOut(LocalDate.now().plus(4, ChronoUnit.DAYS));

        assertThrows(MaximumStayExceedException.class, () -> {
            reservationService.makeReservation(expectedBookingRequest);
        });
    }

    @Test
    public void givenNewBooking_WhenBookingRangeDateInvalid_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        LocalDate invalidDate = LocalDate.now().plus(1, ChronoUnit.DAYS).plus(1, ChronoUnit.MONTHS);
        expectedBookingRequest.setCheckIn(invalidDate);
        expectedBookingRequest.setCheckOut(invalidDate.plus(1, ChronoUnit.DAYS));

        assertThrows(MaximumReservAdvanceExceedException.class, () -> {
            reservationService.makeReservation(expectedBookingRequest);
        });
    }

    @Test
    public void givenNewBooking_WhenReservationExists_ItShouldNotSave() throws ApiException {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();

        when(
            reservationRepository.findByOwnerIdDocAndDateAndStatus(
                any(String.class), any(LocalDate.class), any(LocalDate.class)
            )
        ).thenReturn(ReservationFactory.sample());

        reservationService.makeReservation(expectedBookingRequest);

        verify(roomRepository, times(0)).findAndLockByCheckIn(any());
        verify(reservationRepository, times(0)).save(any());
    }

    @Test
    public void givenNewBooking_WhenNoRoomAvailable_ItShouldThrowException() throws ApiException {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();

        when(
            reservationRepository.findByOwnerIdDocAndDateAndStatus(
                any(String.class), any(LocalDate.class), any(LocalDate.class)
            )
        ).thenReturn(null);

        when(
            roomRepository.findAndLockByCheckIn(any(LocalDate.class))
        ).thenReturn(Collections.emptyList());

        assertThrows(NoRoomAvailableException.class, () -> {
            reservationService.makeReservation(expectedBookingRequest);
        });
    }

    @Test
    public void givenBookingUpdate_WhenValid_ItShouldBeUpdated() throws ApiException {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        List<Room> expectedRooms = List.of(RoomFactory.sample());
        Reservation expectedReservation = new Reservation(expectedRooms.get(0), expectedBookingRequest);

        expectedBookingRequest.setOwnerReservationIdDocument("Updated Id");
        expectedBookingRequest.setOwnerReservationName("Updated Name");
        expectedBookingRequest.setAdults(2);
        expectedBookingRequest.setChildren(2);

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        when(
            reservationRepository.save(any(Reservation.class))
        ).thenReturn(null);

        reservationService.updateReservation(expectedReservation.getId(), expectedBookingRequest);

        Mockito.verify(reservationRepository).save(reservationCaptor.capture());
        Reservation result = reservationCaptor.getValue();

        assertEquals(result.getCheckIn(), expectedBookingRequest.getCheckIn());
        assertEquals(result.getCheckOut(), expectedBookingRequest.getCheckOut());
        assertEquals(result.getAdults(), expectedBookingRequest.getAdults());
        assertEquals(result.getChildren(), expectedBookingRequest.getChildren());
        assertEquals(result.getOwnerReservationName(), expectedBookingRequest.getOwnerReservationName());
        assertEquals(result.getOwnerReservationIdDocument(), expectedBookingRequest.getOwnerReservationIdDocument());
    }

    @Test
    public void givenBookingUpdate_WhenChangeRangeCheckInCheckOut_ItShouldBeUpdated() throws ApiException {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        List<Room> expectedRooms = List.of(RoomFactory.sample());
        Reservation expectedReservation = new Reservation(expectedRooms.get(0), expectedBookingRequest);


        expectedBookingRequest.setCheckIn(LocalDate.now().plus(1, ChronoUnit.DAYS));
        expectedBookingRequest.setCheckOut(LocalDate.now().plus(1, ChronoUnit.DAYS));

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        when(
            roomRepository.findAndLockByCheckIn(any(LocalDate.class))
        ).thenReturn(expectedRooms);

        when(
            reservationRepository.save(any(Reservation.class))
        ).thenReturn(null);

        reservationService.updateReservation(expectedReservation.getId(), expectedBookingRequest);

        Mockito.verify(reservationRepository).save(reservationCaptor.capture());
        Reservation result = reservationCaptor.getValue();

        assertEquals(result.getCheckIn(), expectedBookingRequest.getCheckIn());
        assertEquals(result.getCheckOut(), expectedBookingRequest.getCheckOut());
    }

    @Test
    public void givenBookingUpdate_WhenChangeRangeCheckInCheckOutAndNoRoom_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        List<Room> expectedRooms = List.of(RoomFactory.sample());
        Reservation expectedReservation = new Reservation(expectedRooms.get(0), expectedBookingRequest);

        expectedBookingRequest.setCheckIn(LocalDate.now().plus(1, ChronoUnit.DAYS));
        expectedBookingRequest.setCheckOut(LocalDate.now().plus(1, ChronoUnit.DAYS));

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        when(
            roomRepository.findAndLockByCheckIn(any(LocalDate.class))
        ).thenReturn(Collections.emptyList());

        assertThrows(NoRoomAvailableException.class, () -> {
            reservationService.updateReservation(expectedReservation.getId(), expectedBookingRequest);
        });
    }

    @Test
    public void givenBookingUpdate_WhenStatusIsNotAllowed_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();
        List<Room> expectedRooms = List.of(RoomFactory.sample());
        Reservation expectedReservation = new Reservation(expectedRooms.get(0), expectedBookingRequest);

        expectedReservation.setStatus(ReservationStatus.ACTIVE);

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        assertThrows(UpdateNotAllowedException.class, () -> {
            reservationService.updateReservation(expectedReservation.getId(), expectedBookingRequest);
        });
    }

    @Test
    public void givenBookingUpdate_WhenReservationNotExists_ItShouldThrowException() {
        BookingRequest expectedBookingRequest = BookingRequestFactory.sample();

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.empty());

        assertThrows(ReservationNotFoundException.class, () -> {
            reservationService.updateReservation("123", expectedBookingRequest);
        });
    }

    @Test
    public void givenCanceling_WhenReservationNotExists_ItShouldThrowException() {
        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.empty());

        assertThrows(ReservationNotFoundException.class, () -> {
            reservationService.cancelReservation("123");
        });
    }

    @Test
    public void givenCanceling_WhenValid_ItShouldBeCanceled() throws ApiException {
        Reservation expectedReservation = ReservationFactory.sample();

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        when(
            reservationRepository.save(any(Reservation.class))
        ).thenReturn(null);

        reservationService.cancelReservation(expectedReservation.getId());

        Mockito.verify(reservationRepository).save(reservationCaptor.capture());
        Reservation result = reservationCaptor.getValue();

        assertEquals(result.getStatus(), ReservationStatus.CANCELED);
    }

    @Test
    public void givenCanceling_WhenStatusIsNotAllowedToCancel_ItShouldThrowException() {
        Reservation expectedReservation = ReservationFactory.sample();
        expectedReservation.setStatus(ReservationStatus.EXPIRED);

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        assertThrows(IllegalArgumentException.class, () -> {
            reservationService.cancelReservation(expectedReservation.getId());
        });
    }

    @Test
    public void givenCheckIn_WhenReservationNotExists_ItShouldThrowException() {
        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.empty());

        assertThrows(ReservationNotFoundException.class, () -> {
            reservationService.doCheckIn("123", LocalDate.now());
        });
    }

    @Test
    public void givenCheckIn_WhenCheckInDateIsBeforeReservation_ItShouldThrowException() {
        Reservation expectedReservation = ReservationFactory.sample();
        expectedReservation.setCheckIn(LocalDate.now().plus(1, ChronoUnit.DAYS));

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        assertThrows(IllegalArgumentException.class, () -> {
            reservationService.doCheckIn("123", LocalDate.now());
        });
    }

    @Test
    public void givenCheckIn_WhenStatusIsNotAllowedToCheckIn_ItShouldThrowException() {
        Reservation expectedReservation = ReservationFactory.sample();
        expectedReservation.setStatus(ReservationStatus.ACTIVE);

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        assertThrows(IllegalArgumentException.class, () -> {
            reservationService.doCheckIn("123", LocalDate.now());
        });
    }

    @Test
    public void givenCheckIn_WhenValid_ItShouldUpdateReservation() throws ApiException {
        Reservation expectedReservation = ReservationFactory.sample();
        LocalDate expectedCheckIn = LocalDate.now();

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        when(
            reservationRepository.save(any(Reservation.class))
        ).thenReturn(null);

        reservationService.doCheckIn("123", expectedCheckIn);

        Mockito.verify(reservationRepository).save(reservationCaptor.capture());
        Reservation result = reservationCaptor.getValue();

        assertEquals(result.getStatus(), ReservationStatus.ACTIVE);
        assertEquals(result.getRealCheckIn(), expectedCheckIn);
    }

    @Test
    public void givenCheckOut_WhenStatusIsNotAllowedToCheckOut_ItShouldThrowException() {
        Reservation expectedReservation = ReservationFactory.sample();

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        assertThrows(IllegalArgumentException.class, () -> {
            reservationService.doCheckOut("123", LocalDate.now());
        });
    }

    @Test
    public void givenCheckOut_WhenValid_ItShouldUpdateReservation() throws ApiException {
        Reservation expectedReservation = ReservationFactory.sample();
        expectedReservation.setStatus(ReservationStatus.ACTIVE);
        LocalDate expectedCheckOut = LocalDate.now();

        when(
            reservationRepository.findById(any(String.class))
        ).thenReturn(Optional.of(expectedReservation));

        when(
            reservationRepository.save(any(Reservation.class))
        ).thenReturn(null);

        reservationService.doCheckOut("123", expectedCheckOut);

        Mockito.verify(reservationRepository).save(reservationCaptor.capture());
        Reservation result = reservationCaptor.getValue();

        assertEquals(result.getStatus(), ReservationStatus.INACTIVE);
        assertEquals(result.getRealCheckOut(), expectedCheckOut);
    }
}
