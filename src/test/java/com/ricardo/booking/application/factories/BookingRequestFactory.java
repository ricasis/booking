package com.ricardo.booking.application.factories;

import com.ricardo.booking.application.web.payloads.requests.BookingRequest;

import java.time.LocalDate;

public class BookingRequestFactory {

    public static BookingRequest sample() {
        return new BookingRequest(
            "DOCABC",
            "Client ABC",
            LocalDate.now(),
            LocalDate.now(),
            1,
            0
        );
    }
}
