package com.ricardo.booking.application.factories;

import com.ricardo.booking.domain.entities.Reservation;
import com.ricardo.booking.domain.entities.Room;
import com.ricardo.booking.domain.entities.enums.ReservationStatus;
import de.huxhorn.sulky.ulid.ULID;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class ReservationFactory {

    private static final ULID ulid = new ULID();

    public static Reservation sample() {
        String id = ulid.nextULID();
        Room room = RoomFactory.sample();
        return new Reservation(
            id,
            "Doc " + id,
            "Name " + id,
            LocalDate.now(),
            null,
            LocalDate.now(),
            null,
            1,
            0,
            ReservationStatus.CREATED,
            LocalDateTime.now(),
            null,
            room
        );
    }
}
