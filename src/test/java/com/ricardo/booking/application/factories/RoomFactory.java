package com.ricardo.booking.application.factories;

import com.ricardo.booking.domain.entities.Room;
import de.huxhorn.sulky.ulid.ULID;

import java.time.LocalDateTime;

public class RoomFactory {

    private static final ULID ulid = new ULID();

    public static Room sample() {
        return new Room(
            ulid.nextULID(),
            1,
            1,
            LocalDateTime.now(),
            null,
            null
        );
    }
}
